var express = require('express');
var session = require('cookie-session');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jade = require('jade');
var app = express();
var port=8000; // est pas 8080 sinon erreur

app.use(session({secret: 'motdepasse'}))

.use(function(req, res, next){
  if (typeof(req.session.todolist) == 'undefined') {
    req.session.todolist = [];
  }
  next();
})

.get('/todo', function(req, res) {
  res.render('todo.jade', {todolist: req.session.todolist});
})

.get('/todo/triC', function(req,res) {
  req.session.todolist.sort();
  console.log(req.session.todolist);
  res.redirect('/todo');
})

.get('/todo/triD', function(req,res) {
  req.session.todolist.sort().reverse();
  console.log(req.session.todolist);
  res.redirect('/todo');
})

.post('/todo/ajouter', urlencodedParser, function(req, res) {
  console.log("ajout");
  if (req.body.newtodo != '') {
    req.session.todolist.push(req.body.newtodo+" ");
  }
  res.redirect('/todo');
})

.get('/todo/supprimer/:id', function(req, res) {
  if (req.params.id != '') {
    req.session.todolist.splice(req.params.id, 1);
  }
  res.redirect('/todo');
})

.get('/todo/modif/:id', urlencodedParser, function(req, res){
  var identifiant = "up"+req.params.id;
  console.log(identifiant);
  console.log(req.query);
  console.log(req.query.req.params.id);
  if (req.body.id0 != '') {
    // req.session.todolist.push(req.body.newtodo+" ");

  }
  res.redirect('/todo');
})

.use(function(req, res, next){
  res.redirect('/todo');
})
.listen(port, function () {console.log("Serveur à l'écoute sur le port %d", port);});
